//------------------------------------------------------------
// University of California, San Diego - Bespoke Systems Group
//------------------------------------------------------------
// File: bsg_pll_v6.v
//
// Authors: Luis Vega - lvgutierrez@eng.ucsd.edu
//
//------------------------------------------------------------

module bsg_pll_v6
  (input clk_p_i, clk_n_i
  ,output pll_locked_o
  ,output pll_50_mhz_o);

  // 200MHz ML605 OSC BOARD
  wire bufgds_200_mhz_lo;

  IBUFGDS #
    (.IOSTANDARD("LVDS_25"))
  sysclk_buf
    (.I(clk_p_i)
    ,.IB(clk_n_i)
    ,.O(bufgds_200_mhz_lo));
  
  wire pll_fb_lo, bufg_fb_lo;
  wire pll_50_mhz_lo;

  MMCM_ADV #
    (.BANDWIDTH("OPTIMIZED")
    ,.CLKFBOUT_MULT_F(5)
    ,.CLKFBOUT_PHASE(0.0)
    ,.CLKFBOUT_USE_FINE_PS("FALSE")
    ,.CLKIN1_PERIOD(5)
    ,.CLKIN2_PERIOD(0.0)
    ,.CLKOUT0_DIVIDE_F(20) // 50 MHz
    ,.CLKOUT0_DUTY_CYCLE(0.5)
    ,.CLKOUT0_PHASE(0.0)
    ,.CLKOUT0_USE_FINE_PS("FALSE")
    ,.CLKOUT1_DIVIDE(1)
    ,.CLKOUT1_DUTY_CYCLE(0.5)
    ,.CLKOUT1_PHASE(0.0)
    ,.CLKOUT1_USE_FINE_PS("FALSE")
    ,.CLKOUT2_DIVIDE(1)
    ,.CLKOUT2_DUTY_CYCLE(0.5)
    ,.CLKOUT2_PHASE(0.0)
    ,.CLKOUT2_USE_FINE_PS("FALSE")
    ,.CLKOUT3_DIVIDE(1)
    ,.CLKOUT3_DUTY_CYCLE(0.5)
    ,.CLKOUT3_PHASE(0.0)
    ,.CLKOUT3_USE_FINE_PS("FALSE")
    ,.CLKOUT4_CASCADE("FALSE")
    ,.CLKOUT4_DIVIDE(1)
    ,.CLKOUT4_DUTY_CYCLE(0.5)
    ,.CLKOUT4_PHASE(0.0)
    ,.CLKOUT4_USE_FINE_PS("FALSE")
    ,.CLKOUT5_DIVIDE(1)
    ,.CLKOUT5_DUTY_CYCLE(0.5)
    ,.CLKOUT5_PHASE(0.0)
    ,.CLKOUT5_USE_FINE_PS("FALSE")
    ,.CLKOUT6_DIVIDE(1)
    ,.CLKOUT6_DUTY_CYCLE(0.5)
    ,.CLKOUT6_PHASE(0.0)
    ,.CLKOUT6_USE_FINE_PS("FALSE")
    ,.CLOCK_HOLD("FALSE")
    ,.COMPENSATION("ZHOLD")
    ,.DIVCLK_DIVIDE(1)
    ,.REF_JITTER1(0.0)
    ,.REF_JITTER2(0.0)
    ,.STARTUP_WAIT("FALSE"))
  mmcm_adv_inst
    (.CLKFBOUT(pll_fb_lo)
    ,.CLKFBOUTB()
    ,.CLKFBSTOPPED()
    ,.CLKINSTOPPED()
    ,.CLKOUT0(pll_50_mhz_lo)
    ,.CLKOUT0B()
    ,.CLKOUT1()
    ,.CLKOUT1B()
    ,.CLKOUT2()
    ,.CLKOUT2B()
    ,.CLKOUT3()
    ,.CLKOUT3B()
    ,.CLKOUT4()
    ,.CLKOUT5()
    ,.CLKOUT6()
    ,.DO()
    ,.DRDY()
    ,.LOCKED(pll_locked_o)
    ,.PSDONE()
    ,.CLKFBIN(bufg_fb_lo)
    ,.CLKIN1(bufgds_200_mhz_lo)
    ,.CLKIN2()
    ,.CLKINSEL(1'b1)
    ,.DADDR(7'd0)
    ,.DCLK(1'b0)
    ,.DEN(1'b0)
    ,.DI(16'd0)
    ,.DWE(1'b0)
    ,.PSCLK(1'b0)
    ,.PSEN(1'b0)
    ,.PSINCDEC(1'b0)
    ,.PWRDWN(1'b0)
    ,.RST(1'b0));

  BUFG bufg_fb
    (.I(pll_fb_lo)
    ,.O(bufg_fb_lo));

  BUFG bufg_50_mhz
    (.I(pll_50_mhz_lo)
    ,.O(pll_50_mhz_o));

endmodule
