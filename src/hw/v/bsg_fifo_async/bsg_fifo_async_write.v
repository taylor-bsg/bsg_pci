//------------------------------------------------------------
// University of California, San Diego - Bespoke Systems Group
//------------------------------------------------------------
// File: bsg_fifo_async_write.v
//
// Note: This module is based on the application note from
//       Sunburst Design, "Simulation and Synthesis Techniques
//       for Asynchronous FIFO Design"
//
// Authors: Luis Vega - lvgutierrez@eng.ucsd.edu
//          Qiaoshi Zheng - q5zheng@eng.ucsd.edu
//------------------------------------------------------------

module bsg_fifo_async_write #
  (parameter length_width_p = -1)
  (input clk_i
  ,input reset_i
  ,input enq_i
  ,input [length_width_p : 0] ptr_i
  ,output [length_width_p - 1 : 0] addr_o
  ,output [length_width_p : 0] ptr_o
  ,output full_o);

  // Sync read-pointer with write-clock
  reg [length_width_p : 0] r_ptr_1, r_ptr_2;
  always @(posedge clk_i)
    if(reset_i == 1'b1)
      {r_ptr_2, r_ptr_1} <= 0;
    else
      {r_ptr_2, r_ptr_1} <= {r_ptr_1, ptr_i};

  // Gray/bin pointers
  reg full_r;
  reg [length_width_p : 0] gray_ptr_r, bin_ptr_r;
  wire [length_width_p : 0] gray_next, bin_next, full_condition;

  assign bin_next = bin_ptr_r + (enq_i & ~full_r);
  assign gray_next = (bin_next >> 1) ^ bin_next;

  always @(posedge clk_i)
    if(reset_i == 1'b1)
      {bin_ptr_r, gray_ptr_r} <= 0;
    else
      {bin_ptr_r, gray_ptr_r} <= {bin_next, gray_next};

  // Full
  assign full_condition = {~r_ptr_2[length_width_p : length_width_p - 1], r_ptr_2[length_width_p - 2 : 0]};

  always @(posedge clk_i)
    if (reset_i == 1'b1 || gray_next != full_condition)
      full_r <= 1'b0;
    else
      full_r <= 1'b1;

  // Outputs
  assign addr_o = bin_ptr_r[length_width_p - 1 : 0];
  assign ptr_o = gray_ptr_r;
  assign full_o = full_r;

  // Debug
  always @(posedge clk_i)
    if(enq_i == 1'b1 && full_r == 1'b1)
      $display("ERROR: Queuing full FIFO %t", $time);

endmodule
