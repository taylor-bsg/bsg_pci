//------------------------------------------------------------
// University of California, San Diego - Bespoke Systems Group
//------------------------------------------------------------
// File: bsg_fifo_async_mem.v
//
// Note: This module is based on the application note from
//       Sunburst Design, "Simulation and Synthesis Techniques
//       for Asynchronous FIFO Design"
//
// Authors: Luis Vega - lvgutierrez@eng.ucsd.edu
//          Qiaoshi Zheng - q5zheng@eng.ucsd.edu
//------------------------------------------------------------

module bsg_fifo_async_mem #
  (parameter width_p = -1
  ,parameter length_width_p = -1)
  (input w_clk_i
  ,input w_full_i
  ,input w_enq_i
  ,input [length_width_p - 1 : 0] w_addr_i
  ,input [width_p - 1 : 0] w_data_i
  ,input [length_width_p - 1 : 0] r_addr_i
  ,output [width_p - 1 : 0] r_data_o);

  localparam lg_width_lp = 1 << length_width_p;

  reg [width_p-1:0] mem [0 : lg_width_lp - 1];

  assign r_data_o = mem[r_addr_i];

  always @(posedge w_clk_i)
    if (w_enq_i == 1'b1 && w_full_i == 1'b0)
      mem[w_addr_i] <= w_data_i;

endmodule
