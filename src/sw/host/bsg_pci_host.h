#ifndef __BSG_PCI_HOST_H
#define __BSG_PCI_HOST_H

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <list>
#include <vector>
#include <unistd.h>
#include <dlfcn.h>

#define BSG_PCI_CHANNEL_NUMBER 0x7fc
#define BSG_PCI_HOST_RESET 0x7f8
#define BSG_PCI_TEST_REG 0x7f4
#define BSG_PCI_STATUS_REG 0x7f0

#define kPCIE_DEVICE "/dev/bsg_pci0"

#define DEBUG 0

using namespace std;

#define verify_iom(x,y) do { if (x) ; else {fprintf y; fprintf(stderr," [failed (" #x")] [%s : %d]\n",__FILE__,__LINE__); abort(); }} while (0)

struct bsg_pci_channel {
    unsigned int * bsg_pci_l2f_status_reg;  // linux to fpga status register
    unsigned int * bsg_pci_l2f_fifo;        // linux to fpga receiver FIFO
    unsigned int * bsg_pci_f2l_status_reg;  // fpga to linux status register
    unsigned int * bsg_pci_f2l_fifo;        // fpga to linux transmit FIFO
};

class bsg_pci_device
{
private:
    void * bpd_base_addr;
    unsigned int * bpd_channel_number;
    unsigned int * bpd_host_reset;
    unsigned int * bpd_test_reg;
    unsigned int * bpd_status_reg;
    int channel_number;
    int fd;  // file descripter

    // Pointer to channels
    bsg_pci_channel * bpd_channel;

    ofstream myfile;

    vector<list<unsigned int> > * in_list;

    vector<int> * l2f_status_reg;
    int current_channel;

    void open_device (const char *device);
    void initiate_connection();

public:
    bsg_pci_device(const char *device);

    ~bsg_pci_device();

    void handle_pci_io(int channel);

    int  read_packet_async (int channel, unsigned int  &t);

    void read_packet_blocking(int channel, unsigned int &t, int nicely);

    int  peek_packet_async (int channel, unsigned int  &t);

    void write_packet_async (int channel, unsigned int t);

    int write_packet_if_less_than(int channel, unsigned int t, int n);

    // Some usefull functions
    int check_after_sleep(int usec);
};

#endif
